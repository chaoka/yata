const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');

module.exports = {
    devtool: 'cheap-module-source-map',
    entry: './src/js/index.js',
    output: {
        filename: './dist/bundle.js',
    },
    module: {
        rules: [
            { test: /\.html$/, use: ['html-loader'] }, 
            { test: /\.sass$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer]
                        }
                    }, 'sass-loader']
                })
            }, 
            { test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader', {
                        loader: 'image-webpack-loader',
                        query: {
                            progressive: true,
                            optimizationLevel: 7,
                            interlaced: false,
                            pngquant: {
                                quality: '65-90',
                                speed: 4
                            }
                        }
                    }
                ]
            },
            { test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: { presets: ['es2015'] }
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('./dist/style.css')
    ]
};
