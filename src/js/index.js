import style from '../sass/main.sass';

import angular from 'angular';
import uiRouter from 'angular-ui-router';

import routes from './routes';
import services from './services';
import components from './components';

angular.module('app',[uiRouter, services.name, components.name])
    .config(routes)