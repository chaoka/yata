import template from './navbar.html';
import controller from './navbar.controller';

const navbar = angular.module('app.components.navbar', []);

navbar.component('navbar', {
    template: template,
    controller: controller
});

export default navbar;
