import navbar from './navbar/navbar';
import todolist from './todolist/todolist';
import todo from './todo/todo';
import todoinput from './todoinput/todoinput';

const components = angular.module('app.components', [
    navbar.name,
    todolist.name,
    todo.name,
    todoinput.name
]);

export default components;