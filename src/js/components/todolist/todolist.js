import template from './todolist.html';
import controller from './todolist.controller';

const todolist = angular.module('app.components.todolist', []);

todolist.component('todolist', {
    template: template,
    controller: controller
});

export default todolist;
