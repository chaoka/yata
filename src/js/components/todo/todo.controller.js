export default function(todosService){
    const vm = this;
    vm.removeTodo = (i) => todosService.removeTodo(i);
}