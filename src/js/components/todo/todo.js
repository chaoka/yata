import template from './todo.html';
import controller from './todo.controller';

const todo = angular.module('app.components.todo', []);

todo.component('todo', {
    template: template,
    controller: controller,
    bindings: {
        todo: '<',
        index: '<'
    }
});

export default todo;
