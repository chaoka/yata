export default function(todosService){
    const vm = this;
    vm.addTodo = () => {
        todosService.addTodo(vm.todo);
        vm.todo = '';
    }
}