import template from './todoinput.html';
import controller from './todoinput.controller';

const todoinput = angular.module('app.components.todoinput', []);

todoinput.component('todoinput', {
    template: template,
    controller: controller
});

export default todoinput;
