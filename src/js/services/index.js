import todosService from './todos.service.js'

const services = angular.module('app.services',[todosService.name]);

export default services;