const todosService = angular.module('app.services.todos',[])

todosService.service('todosService', function(){

    let todos = null;

    return {
        fetch: fetch,
        getTodos: getTodos,
        addTodo: addTodo,
        removeTodo: removeTodo
    }

    function fetch() {
        todos = ['one','two','three'];
        return Promise.resolve(todos);
    }

    function getTodos() {
        if (todos) return Promise.resolve(todos)
        return fetch();
    }

    function addTodo(todo) {
        todos.push(todo)
    }

    function removeTodo(i){
        todos.splice(i, 1)
    }
})

export default todosService;