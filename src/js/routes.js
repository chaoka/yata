export default function($stateProvider, $urlRouterProvider){
    $stateProvider
        .state('todolist', {
            name: 'todolist',
            url: '/todolist',
            template: '<todoinput></todoinput><todolist></todolist>'
        })
        $urlRouterProvider.otherwise('/todolist')
}